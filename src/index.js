const express = require('express');
const path = require('path');
const passport = require('passport');
const expressSession = require('express-session');
const cookieParser = require('cookie-parser');
const expressLiquid = require('express-liquid');
const bodyParser = require('body-parser');
const FileStore = require('session-file-store')(expressSession);

const router = require('./routes');
const port = process.env.PORT || 3000;

const app = express();
app.use(require('connect-flash')());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())
app.use(expressSession({
  secret: 'lkimdfglkdgf',
  resave: false,
  saveUninitialized: true,
  store: new FileStore({ logFn: function () { } }), // stop it spamming error logs when initialising
  // cookie: { secure: true }
}));
app.use(cookieParser());

// template engine
app.set('view engine', 'liquid');
app.engine('liquid', expressLiquid());
app.use(expressLiquid.middleware);
app.set('views', path.join(__dirname, './views'));

// setup passport using our config
require('./passport')(passport);
app.use(passport.initialize());
app.use(passport.session());

// use the routes defined in routes.js
app.use(router);

app.listen(port, () => {
  console.log(`Running on localhost:${port}`)
})

module.exports = app;