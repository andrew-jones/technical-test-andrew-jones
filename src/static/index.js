/**
 * Let user switch between register and login
 */
function loginRegisterSwitch() {
  document.querySelector('#switch-to-register').addEventListener('click', function (e) {
    document.querySelector('#login').classList.remove('show');
    document.querySelector('#register').classList.add('show');
  });
  document.querySelector('#switch-to-login').addEventListener('click', function (e) {
    document.querySelector('#register').classList.remove('show');
    document.querySelector('#login').classList.add('show');
  });
}

/**
 * Since there's two username and password fields, updating one updates the other
 */
function loginRegisterInput() {
  const registerUsername = document.querySelector('#register input[name="username"]');
  const loginUsername = document.querySelector('#login input[name="username"]');
  const registerPassword = document.querySelector('#register input[name="password"]');
  const loginPassword = document.querySelector('#login input[name="password"]');

  registerUsername.addEventListener('change', function () {
    loginUsername.value = registerUsername.value;
  });
  loginUsername.addEventListener('change', function () {
    registerUsername.value = loginUsername.value;
  });

  registerPassword.addEventListener('change', function () {
    loginPassword.value = registerPassword.value;
  });
  loginPassword.addEventListener('change', function () {
    registerPassword.value = loginPassword.value;
  });
}

window.onload = function () {
  if (document.querySelector('body.page-home')) {
    const numberInput = document.querySelector('#number');
    const words = document.querySelector('#words');
    words.textContent = numberToWords(numberInput.value);
    numberInput.addEventListener('input', function (e) {
      words.textContent = numberToWords(numberInput.value);
    });
  }

  if (document.querySelector('body.page-auth')) {
    loginRegisterSwitch();
    loginRegisterInput();
  }
}