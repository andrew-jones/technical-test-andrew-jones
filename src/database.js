/**
 * Usually this would actually connect up to a db. For this test I'm just using JSON in memory,
 * that gets written to a file that's saved on each insertion, and is loaded at runtime.
 */

const fs = require('fs');

let users = [];
try {
  const contents = fs.readFileSync('db.json');
  users = JSON.parse(contents);
} catch (err) {
  // console.log(err)
  // if file wasn't read correctly (corrupt or non-existant), then we start with the standard users
  // but lets create the file for later
  fs.writeFileSync('db.json', JSON.stringify(users));
}


module.exports = {
  // async in case this turns into an actual db query
  getUserById: async (id) => {
    return users.find(user => user.id === id);
  },

  getUserByUsername: async (username) => {
    return users.find(user => user.username.toLowerCase() === username.toLowerCase());
  },

  // add to list of users, and write to 'database'
  insertUser: async (username, password) => {
    const user = { username, password, id: Date.now() };
    users.push(user);

    try {
      fs.writeFileSync('db.json', JSON.stringify(users));
    } catch (err) {
      console.log(err);
      // we don't really need to do anything here
    }

    return user;
  }
}